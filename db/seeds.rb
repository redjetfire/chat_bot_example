require './lib/models/question.rb'

questions_list = [
  {
    question: "How can we reach out to you?",
    identifier: 'reach_out_by',
    options: ['phone', 'email', 'do not contact me'],
    keyword_pointers: {
      'phone': 'phone_value',
      'email': 'email_value',
      'do_not_contact_me': 'quit_app'
    }
  },
  {
    question: "Please type your phone number",
    identifier: 'phone_value',
    keyword_pointers: {
      'any': 'reach_time'
    }
  },
  {
    question: "What is the best time we can reach out to you?",
    identifier: 'reach_time',
    options: ['asap', 'morning', 'afternoon', 'evening'],
    keyword_pointers: {
      'asap': 'verify_details_phone',
      'morning': 'verify_details_phone',
      'afternoon': 'verify_details_phone',
      'evening': 'verify_details_phone',
    }
  },
  {
    question: "We are going to contact you using",
    identifier: 'verify_details_phone',
    options: ['yes please', 'sorry wrong phone'],
    require_values_from: ['reach_out_by', 'phone_value'],
    keyword_pointers: {
      'yes_please': 'save_and_exit',
      'sorry_wrong_phone': 'phone_value'
    }
  },
  {
    question: "We are going to contact you using",
    identifier: 'verify_details_email',
    options: ['yes please', 'sorry wrong email'],
    require_values_from: ['reach_out_by', 'email_value'],
    keyword_pointers: {
      'yes_please': 'save_and_exit',
      'sorry_wrong_email': 'email_value'
    }
  },
  {
    question: "Please type your email adress",
    identifier: 'email_value',
    keyword_pointers: {
      'any': 'verify_details_email'
    }
  },
  {
    question: "Said to hear that. Whenever you change your mind - feel free to send me a message.\nPress any key to exit.",
    identifier: 'quit_app'
  },
  {
    question: "Thank you. We will contact you soon!\nPress Enter to exit.",
    identifier: 'save_and_exit'
  }
]

questions_list.each do |attributes|
  Question.create(attributes)
end
