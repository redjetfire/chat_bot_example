class CreateQuestion < ActiveRecord::Migration
  def self.up
    create_table :questions do |t|
      t.string :question
      t.string :identifier
      t.text :require_values_from
      t.text :keyword_pointers
      t.text :options
    end
  end

  def self.down
    drop_table :questions
  end
end
