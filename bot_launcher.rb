#!/usr/bin/env ruby
require './lib/initializers/active_record_initializer.rb'
require './lib/chat_bot/conversation.rb'
require './lib/chat_bot/question_preparator.rb'
require './lib/chat_bot/reaction.rb'
require './lib/chat_bot/user_object.rb'
require './lib/models/user.rb'
require './lib/models/question.rb'
require './lib/models/answer.rb'
require './lib/extensions/object.rb'
require './lib/extensions/string.rb'

module ChatBot
  class BotLauncher
    INITIALIZE_MESSAGE = "I can help you with answers on all your related questions and help to find a great job!"
    INITIALIZE_QUESTION = "Do you want to talk? : "

    def self.call
      print "#{INITIALIZE_MESSAGE}\n#{INITIALIZE_QUESTION}".green
      while true
        command = user_input
        case command.downcase
        when "talk", "lets talk", "let's talk", "yes", "y"
          Conversation.new.call
        when "exit", "quit", "no"
          puts "Exiting...".red
          break
        end
      end
    end
  end
end

ChatBot::BotLauncher.call
