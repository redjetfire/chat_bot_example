require_relative 'spec_helper.rb'

describe ChatBot::Reaction do
  describe 'methods' do
    before(:all) do
      @user = User.create(name: 'Test')
      @question = Question.where(identifier: 'reach_out_by').first
      @next_question = Question.where(identifier: 'phone_value').first
    end

    before(:each) do
      @reaction = ChatBot::Reaction.new(@user)
    end

    describe "#process" do
      it "set user response, question and make a decision" do
        expect(@reaction).to receive(:set_response).with("Hello").and_call_original
        expect(@reaction).to receive(:set_question).with(@question).and_call_original
        expect(@reaction).to receive(:decision_process).and_return(1)

        @reaction.process("Hello", @question)

        expect(@reaction.response).to eq("Hello")
        expect(@reaction.question).to eq(@question)
        expect(@reaction.decision).to eq(1)
      end
    end

    describe "#decision_process" do
      it "save response and set next question" do
        allow(@reaction).to receive(:response).and_return('do not contact me')

        expect(@question).to receive(:pointers).with('do not contact me').and_call_original
        expect(@reaction).to receive(:response_value?).and_call_original
        expect(@reaction).to receive(:save_response).and_return(true)
        expect(@reaction).to receive(:set_next_question).and_return(@next_question)

        @reaction.process('do not contact me', @question)

        expect(@reaction.decision).to eq(1)
      end

      it "validate invalid command" do
        allow(@reaction).to receive(:response).and_return('do not contact me')
        allow(@question).to receive(:pointers).with('do not contact me').and_return(false)
        allow(@reaction).to receive(:response_value?).and_return(false)
        allow(@question).to receive(:empty_pointers?).and_return(false)

        expect(@reaction).not_to receive(:set_next_question)
        expect { @reaction.process('this is wrong message', @question) }.to output("Invalid answer or no value entered! Try again.\n".red).to_stdout
        expect(@reaction.decision).to eq(0)
      end

      it "exit the conversation" do
        allow(@reaction).to receive(:response).and_return('do not contact me')

        expect(@question).to receive(:pointers).with('do not contact me').and_return(false)
        expect(@question).to receive(:empty_pointers?).and_return(true)
        expect(@reaction).not_to receive(:set_next_question)
        expect { @reaction.process('do not contact me', @question) }.to raise_exception(SystemExit)
      end
    end
  end
end
