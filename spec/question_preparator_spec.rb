require_relative 'spec_helper.rb'

describe ChatBot::QuestionPreparator do
  describe 'methods' do
    before(:all) do
      @user = User.create(name: 'Test')
      @question = Question.create(
        question: "Welcome user",
        identifier: 'welcome',
        keyword_pointers: {
          'any': 'how_are_you'
        }
      )
      @require_values_from_question = Question.create(
        question: "We will contact you using",
        identifier: 'contact',
        require_values_from: ['user_phone'],
        keyword_pointers: {
          'yes': 'save'
        }
      )
      @question_with_options = Question.create(
        question: "What is the best time we can reach out to you?",
        identifier: 'reach_out_time',
        options: ['asap', 'morning', 'afternoon', 'evening'],
        keyword_pointers: {
          'any': 'continue'
        }
      )
      @answer = Answer.create(user: @user, question: @question, answer: "096-86-94-350")
    end

    before(:each) do
      @question_preparator = ChatBot::QuestionPreparator.new(@user)
    end

    describe '#call' do
      it 'form a question by adding : symbol' do
        allow(@question_preparator).to receive(:call).and_call_original
        allow(@question_preparator).to receive(:set_data).and_call_original
        allow(@question_preparator).to receive(:question_with_separator).and_call_original
        allow(Answer).to receive(:where).and_return([])
        allow_any_instance_of(String).to receive(:green).and_call_original

        expect(@question_preparator).not_to receive(:question_with_answers)
        expect(@question_preparator).not_to receive(:question_with_options)

        expect(@question_preparator.call(@question)).to eq('Welcome user : '.green)
      end

      it 'form a question by adding options list' do
        allow(@question_preparator).to receive(:call).and_call_original
        allow(Answer).to receive(:where).and_return([])
        allow_any_instance_of(String).to receive(:green).and_call_original

        expect(@question_preparator).not_to receive(:question_with_answers)
        expect(@question_preparator).to receive(:question_with_options).and_call_original

        expect(@question_preparator.call(@question_with_options)).to eq('What is the best time we can reach out to you?(asap, morning, afternoon, evening) : '.green)
      end

      it 'form a question by adding specific values from user response' do
        allow(@question_preparator).to receive(:call).and_call_original
        allow(Answer).to receive(:where).and_return([@answer])
        allow_any_instance_of(String).to receive(:green).and_call_original

        expect(@question_preparator).to receive(:question_with_answers).and_call_original
        expect(@question_preparator).not_to receive(:question_with_options)

        expect(@question_preparator.call(@require_values_from_question)).to eq('We will contact you using 096-86-94-350 : '.green)
      end
    end
  end
end
