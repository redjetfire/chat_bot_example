require_relative 'spec_helper.rb'

describe ChatBot::Conversation do
  describe 'methods' do
    before(:all) do
      @user = User.create(name: 'Test')
      @question = Question.where(identifier: 'reach_out_by').first
      @next_question = Question.where(identifier: 'phone_value').first
      @reaction = ChatBot::Reaction.new(@user)
      @question_preparator = ChatBot::QuestionPreparator.new(@user)
    end

    before(:each) do
      @conversation = ChatBot::Conversation.new
    end

    describe '#call' do
      it 'call #set_user, #prepare, #start' do
        expect(@conversation).to receive(:set_user).and_return(@user)
        expect(@conversation).to receive(:prepare).and_call_original
        expect(@conversation).to receive(:start).and_return(true)

        @conversation.call

        expect(@conversation.question).not_to be_nil
        expect(@conversation.reaction).not_to be_nil
        expect(@conversation.question_preparator).not_to be_nil
      end
    end

    describe '#start' do
      it 'print a question' do
        allow_any_instance_of(String).to receive(:green).and_call_original

        expect(@conversation).to receive(:question_preparator).and_return(@question_preparator)
        expect(@question_preparator).to receive(:call).and_return(@question.question)
        expect(@conversation).to receive(:process).and_return(true)
        expect { @conversation.start }.to output(@question.question).to_stdout
      end
    end

    describe '#process' do
      it 'react on user response' do
        allow(@conversation).to receive(:user_input).and_return("Hello bot!")
        allow(@conversation).to receive(:reaction).and_return(@reaction)

        expect_any_instance_of(ChatBot::Reaction).to receive(:process).and_return(true)
        expect(@conversation).to receive(:continue).and_return(true)

        @conversation.process
      end
    end

    describe '#continue' do
      before(:each) do
        @conversation.question = @question
      end

      it 'set next question and continue the conversation' do
        allow(@conversation).to receive(:reaction).and_return(@reaction)
        allow_any_instance_of(ChatBot::Reaction).to receive(:decision).and_return(1)
        allow_any_instance_of(ChatBot::Reaction).to receive(:next_question).and_return(@next_question)

        expect(@conversation).to receive(:start).and_return(true)

        @conversation.continue

        expect(@conversation.question).to eq(@next_question)
      end

      it 'repeat current question' do
        allow(@conversation).to receive(:reaction).and_return(@reaction)
        allow_any_instance_of(ChatBot::Reaction).to receive(:decision).and_return(0)

        expect(@conversation).to receive(:start).and_return(true)
        expect_any_instance_of(ChatBot::Reaction).not_to receive(:next_question)

        @conversation.continue

        expect(@conversation.question).to eq(@question)
      end
    end
  end
end
