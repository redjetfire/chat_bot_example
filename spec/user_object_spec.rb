require_relative 'spec_helper.rb'

describe ChatBot::UserObject do
  describe 'methods' do
    describe "#call" do
      it "ask you a name" do
        allow(ChatBot::UserObject).to receive(:user_input).and_return("Hello")

        expect(ChatBot::UserObject).to receive(:call).and_call_original
        expect { ChatBot::UserObject.call }.to output("Enter your name : ".green).to_stdout
      end

      it "it read your response name and create a new user record" do
        allow(ChatBot::UserObject).to receive(:print).and_return(true)

        expect(ChatBot::UserObject).to receive(:user_input).and_return("My_name")
        expect(User).to receive(:create).and_call_original

        ChatBot::UserObject.call

        expect(User.last.name).to eq("My_name")
      end
    end
  end
end
