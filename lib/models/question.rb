require 'active_record'

class Question < ActiveRecord::Base
  serialize :keyword_pointers, Hash
  serialize :options , Array
  serialize :require_values_from, Array

  def empty_pointers?
    keyword_pointers.empty?
  end

  def pointers(response = nil)
    keyword_pointers[response.downcase.gsub(' ', '_').to_sym] || keyword_pointers[:any]
  end
end
