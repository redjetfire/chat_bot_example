module ChatBot
  class Reaction
    attr_accessor :next_question, :decision, :response, :question, :user

    def initialize(user)
      @user = user
    end

    def process(user_response, bot_question)
      set_response(user_response)
      set_question(bot_question)
      self.decision = decision_process
    end

    # 1 - Next question
    # 0 - Same question (Invalid answer or no value entered)
    def decision_process
      if question.pointers(response) && response_value?
        save_response
        set_next_question
        1
      elsif question.empty_pointers?
        exit
      else
        print "Invalid answer or no value entered! Try again.\n".red
        0
      end
    end

    private
    def set_response(user_response)
      self.response = user_response
    end

    def set_question(bot_question)
      self.question = bot_question
    end

    def save_response
      answer = Answer.find_or_create_by(user: user, question: question)
      answer.update_attributes(answer: response)
    end

    def set_next_question
      self.next_question = Question.find_by(identifier: question.pointers(@response))
    end

    def response_value?
      response.length > 0
    end
  end
end
