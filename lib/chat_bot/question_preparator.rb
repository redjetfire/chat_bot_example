module ChatBot
  class QuestionPreparator

    attr_accessor :question_obj, :question_string

    def initialize(user)
      @user = user
    end

    def call(question)
      set_data(question)
      question_with_answers if @question_obj.require_values_from.any?
      question_with_options if @question_obj.options.any?
      question_with_separator
    end

    def question_with_answers
      @answers = Answer.where(user: @user, question: Question.where(identifier: @question_obj.require_values_from))
      @answers.map(&:answer).each { |val| @question_string += " #{val}" }
    end

    def question_with_options
      @question_string += question_options_list(@question_obj) if @question_obj.options.count > 1
    end

    def question_with_separator
      @question_string += " : "
      @question_string.green
    end

    private
    def question_options_list(question)
      "(#{question.options.join(', ')})"
    end

    def set_data(question)
      @question_obj = question
      @question_string = @question_obj.question
    end
  end
end
