module ChatBot
  class Conversation
    attr_accessor :user, :question, :reaction, :question_preparator

    def call
      set_user
      prepare
      start
    end

    def start
      print question_preparator.call(question)
      process
    end

    def process
      response = user_input
      reaction.process(response, @question)
      continue
    end

    def continue
      self.question = case reaction.decision
                      when 1
                        reaction.next_question
                      when 0
                        self.question
                      end
      start
    end

    private
    def set_user
      self.user = UserObject.call
    end

    def prepare
      self.question            = Question.first
      self.reaction            = Reaction.new(@user)
      self.question_preparator = QuestionPreparator.new(@user)
    end
  end
end
