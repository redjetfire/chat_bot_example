require 'active_record'
require 'yaml'

conf = YAML.load_file('db/config.yml')
ActiveRecord::Base.establish_connection conf[ENV['DB']]
